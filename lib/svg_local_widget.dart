import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:jovial_svg/jovial_svg.dart';

class SvgLocalWidget extends StatelessWidget {
  final double? height;
  final double? width;

  final String assetPath;

  const SvgLocalWidget({
    this.height,
    this.width,
    required this.assetPath,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: ScalableImageWidget.fromSISource(
        si: ScalableImageSource.fromSvg(
          rootBundle,
          assetPath,
        ),
        fit: BoxFit.cover,
        reload: false,
      ),
    );
  }
}
