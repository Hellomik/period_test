import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:period_test/navigator/navigator_bloc.dart';
import 'package:period_test/pages/choice_page.dart';
import 'package:period_test/pages/date_of_birth_page.dart';
import 'package:period_test/pages/value_page.dart';

/// IF YOU WANT IMPLEMENT LOADER PAGE YOU CAN USE ROUTER AND MIX IT WITH COMPLETER
/// You can use MultiListener and implement all navifation in your bloc depens on Task indeed.

class PeriodRouter<Object> extends RouterDelegate<Object>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin {
  @override
  final GlobalKey<NavigatorState>? navigatorKey;

  NavigatorBloc? navigatorBloc;

  PeriodRouter({this.navigatorKey});

  @override
  Widget build(BuildContext context) {
    navigatorBloc = BlocProvider.of<NavigatorBloc>(context, listen: false);
    return BlocBuilder<NavigatorBloc, NavigatorBlocState>(
      bloc: navigatorBloc,
      builder: (context, state) {
        return Navigator(
          onPopPage: (route, result) {
            // log("CEHCL");
            // BlocProvider.of<NavigatorBloc>(context, listen: false)
            //     .add(const SwitchToInitNavigatorEvent());
            return false;
          },
          key: navigatorKey,
          pages: [
            // DEPENDS on WHAT YOU WANT

            const MaterialPage(
              child: ChoicePage(),
            ),
            if (state is ChooseNavigatorBlocState)
              MaterialPage(
                child: DateOfBirthPage(
                  status: state.status,
                ),
              ),
            if (state is ValueNavigatorBlocState)
              MaterialPage(
                child: ValuePage(
                  status: state.status,
                  year: state.year,
                ),
              ),
          ],
        );
      },
    );
  }

  @override
  Future<bool> popRoute() async {
    navigatorBloc?.add(const SwitchToInitNavigatorEvent());
    return true;
  }

  @override
  Future<void> setNewRoutePath(configuration) async {}
}
