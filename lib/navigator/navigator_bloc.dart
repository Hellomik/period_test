import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:period_test/navigator/model/status.enum.dart';

part 'navigator_event.dart';
part 'navigator_state.dart';
part 'reaction/choice_reaction.dart';
part 'reaction/init_reaction.dart';
part 'reaction/set_value_reaction.dart';

class NavigatorBloc extends Bloc<NavigatorEvent, NavigatorBlocState> {
  NavigatorBloc() : super(const EmptyNavigatorBlocState()) {
    on<SwitchToChoiceNavigatorEvent>(switchToChoiceNavigatorEvent);
    on<SwitchToInitNavigatorEvent>(switchToInitNavigatorEvent);
    on<SetValueNavigatorEvent>(setValueRaction);
  }
}
