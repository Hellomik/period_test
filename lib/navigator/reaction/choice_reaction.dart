part of '../navigator_bloc.dart';

extension ChoiceReaction on NavigatorBloc {
  void switchToChoiceNavigatorEvent(
    SwitchToChoiceNavigatorEvent event,
    Emitter<NavigatorBlocState> emit,
  ) {
    emit(ChooseNavigatorBlocState(event.status));
  }
}
