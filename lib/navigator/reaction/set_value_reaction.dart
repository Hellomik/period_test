part of '../navigator_bloc.dart';

extension SetValueRaction on NavigatorBloc {
  void setValueRaction(
    SetValueNavigatorEvent event,
    Emitter<NavigatorBlocState> emit,
  ) {
    emit(ValueNavigatorBlocState(event.year, event.status));
  }
}
