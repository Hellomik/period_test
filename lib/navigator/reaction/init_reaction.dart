part of '../navigator_bloc.dart';

extension InitReaction on NavigatorBloc {
  void switchToInitNavigatorEvent(
    SwitchToInitNavigatorEvent event,
    Emitter<NavigatorBlocState> emit,
  ) {
    emit(const EmptyNavigatorBlocState());
  }
}
