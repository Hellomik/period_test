enum StatusValue {
  getPregnant('getPregnant'),
  track('track');

  const StatusValue(this.value);
  final String value;
}
