part of 'navigator_bloc.dart';

abstract class NavigatorEvent {
  const NavigatorEvent();
}

class SwitchToInitNavigatorEvent extends NavigatorEvent {
  const SwitchToInitNavigatorEvent();
}

class SwitchToChoiceNavigatorEvent extends NavigatorEvent {
  final StatusValue status;
  const SwitchToChoiceNavigatorEvent(this.status);
}

class SetValueNavigatorEvent extends NavigatorEvent {
  final int year;
  final StatusValue status;
  const SetValueNavigatorEvent(this.year, this.status);
}
