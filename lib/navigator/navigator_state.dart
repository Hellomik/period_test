part of 'navigator_bloc.dart';

abstract class NavigatorBlocState {
  const NavigatorBlocState();
}

// We want add page ? or we want just switch between pages
class EmptyNavigatorBlocState extends NavigatorBlocState {
  const EmptyNavigatorBlocState();
}

class ChooseNavigatorBlocState extends NavigatorBlocState {
  final StatusValue status;

  const ChooseNavigatorBlocState(this.status);
}

class ValueNavigatorBlocState extends NavigatorBlocState {
  final StatusValue status;
  final int year;

  const ValueNavigatorBlocState(this.year, this.status);
}
