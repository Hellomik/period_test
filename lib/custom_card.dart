import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final VoidCallback? onTap;
  final String title;
  final String subTitle;

  const CustomButton({
    required this.title,
    required this.subTitle,
    this.onTap,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20),
        decoration: const BoxDecoration(
          color: Color(0xffFFEFEF),
          borderRadius: BorderRadius.all(
            Radius.circular(30),
          ),
        ),
        width: double.infinity,
        padding: const EdgeInsets.only(
          left: 12,
          right: 16,
          top: 30,
          bottom: 30,
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge
                        ?.copyWith(color: Colors.black),
                  ),
                  Text(
                    subTitle,
                    style: Theme.of(context).textTheme.bodyMedium,
                  )
                ],
              ),
            ),
            const Icon(Icons.chevron_right),
          ],
        ),
      ),
    );
  }
}
