import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:period_test/custom_card.dart';
import 'package:period_test/gen/assets.gen.dart';
import 'package:period_test/navigator/model/status.enum.dart';
import 'package:period_test/navigator/navigator_bloc.dart';
import 'package:period_test/pages/date_of_birth_page.dart';
import 'package:period_test/svg_local_widget.dart';

class ChoicePage extends StatelessWidget {
  const ChoicePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Stack(
          children: [
            const Positioned.fill(
              child: SvgLocalWidget(
                assetPath: Assets.back,
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomButton(
                  title: 'Track my period',
                  subTitle: 'contraception and wellbeing',
                  onTap: () {
                    BlocProvider.of<NavigatorBloc>(context, listen: false).add(
                        const SwitchToChoiceNavigatorEvent(StatusValue.track));
                  },
                ),
                const SizedBox(height: 40),
                CustomButton(
                  title: 'Get pregnant',
                  subTitle: 'learn about reproductive health',
                  onTap: () {
                    BlocProvider.of<NavigatorBloc>(context, listen: false).add(
                        const SwitchToChoiceNavigatorEvent(
                            StatusValue.getPregnant));
                  },
                ),
                const SizedBox(height: 40),
              ],
            )
          ],
        ),
      ),
    );
  }
}
