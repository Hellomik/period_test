import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:period_test/custom_card.dart';
import 'package:period_test/gen/assets.gen.dart';
import 'package:period_test/navigator/model/status.enum.dart';
import 'package:period_test/navigator/navigator_bloc.dart';
import 'package:period_test/svg_local_widget.dart';

class DateOfBirthPage extends StatefulWidget {
  final StatusValue status;

  const DateOfBirthPage({required this.status, super.key});

  @override
  State<DateOfBirthPage> createState() => _DateOfBirthPageState();
}

class _DateOfBirthPageState extends State<DateOfBirthPage> {
  FixedExtentScrollController controller = FixedExtentScrollController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          const Positioned.fill(
            child: SvgLocalWidget(assetPath: Assets.period),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Log in your date of birth',
                style: Theme.of(context)
                    .textTheme
                    .headline3
                    ?.copyWith(color: Colors.black),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 20),
              SizedBox(
                height: 200,
                child: ListWheelScrollView.useDelegate(
                  itemExtent: 40,
                  controller: controller,
                  childDelegate: ListWheelChildBuilderDelegate(
                    builder: (context, index) => Text(
                      (1990 + index).toString(),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 40),
              TextButton(
                onPressed: () {
                  BlocProvider.of<NavigatorBloc>(context, listen: false).add(
                      SetValueNavigatorEvent(
                          1990 + controller.selectedItem, widget.status));
                },
                child: Text('NEXT'),
              ),
            ],
          )
        ],
      ),
    );
  }
}
