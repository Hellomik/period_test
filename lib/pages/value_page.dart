import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:period_test/navigator/model/status.enum.dart';

class ValuePage extends StatelessWidget {
  final int year;
  final StatusValue status;

  const ValuePage({required this.year, required this.status, super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Text(status.value),
          const SizedBox(height: 10),
          Text(year.toString()),
        ],
      ),
    );
  }
}
